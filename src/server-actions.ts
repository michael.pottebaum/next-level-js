'use server'
import { dbClient } from "@/db-client"

interface FactsResp {
  id: string;
  text: string;
  source: string;
  source_url: string;
  language: string;
  permalink: string;
}

interface SavedFact {
  id: number;
  text: string;
  created_at: string;
}

export async function getFact() {
  const url = process.env.FACTS_API
  if (!url) throw Error('uh oh the facts api url is a no go')
  const resp = await fetch(url, {
    next: { revalidate: 0 }
  })
  const data: FactsResp = await resp.json()
  return data.text
}

export async function selectFacts() {
  const { rows: savedFacts, } = await dbClient.execute('SELECT * FROM facts')
  return savedFacts as unknown as SavedFact[]
}

export async function insertFact(text: string) {
  const resp = await dbClient.execute({
    sql: 'INSERT INTO facts (text) VALUES(?);',
    args: [text],
  })
  console.log('save resp', resp)
}

