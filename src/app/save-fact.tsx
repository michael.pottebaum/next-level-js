'use client'
import { insertFact } from '@/server-actions'
import { useFormStatus } from 'react-dom'

export function SaveFact({ fact }: { fact: string }) {
  return (
    <section className='flex p-2'>
      <form action={() => insertFact(fact)}>
        <SaveFactBtn />
      </form>
    </section>
  )
}

function SaveFactBtn() {
  const { pending } = useFormStatus()
  return (
    <button disabled={pending} type='submit' className='border border-black p-1'>
      {pending ? 'pending' : 'save'}
    </button>
  )
}

