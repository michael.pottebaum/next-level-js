import { getFact, selectFacts, } from "@/server-actions"
import { SaveFact } from "./save-fact";

export default async function Home() {
  const fact = await getFact()
  const savedFacts = await selectFacts()
  return (
    <main className="flex flex-col items-center p-2 w-full">
      <h1>yo world</h1>
      <p className="p-2">{fact}</p>
      <SaveFact fact={fact} />
      <section>
        <h4>Saved Facts</h4>
        <ul>
          {savedFacts.map(f => (
            <li key={f.id}>{f.text}</li>
          ))}
        </ul>
      </section>
    </main>
  );
}
